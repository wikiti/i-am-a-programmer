define(function() {
    function SaveData() {
        // -- Private fields

        const self = this;
        const data = (function() {
            if (typeof(Storage) !== "undefined")
                return localStorage;

            console.warn("Local storage is not supported");
            return {};
        })();

        const persistence = 'persistence';
        const normal = 'normal';

        // -- Public fields

        // ...

        // -- Private functions

        var getProperty = function(name, type) {
            return data[`${type}/${name}`];
        };

        var setProperty = function(name, value, type) {
            return data[`${type}/${name}`] = value;
        };

        // -- Public functions

        self.persistence = function(name, value, default_value) {
            if(value !== undefined) {
                return setProperty(name, value, persistence);
            }
            else {
                var prop = getProperty(name, persistence);
                if(prop == undefined && default_value != undefined)
                    return setProperty(name, default_value, persistence);

                return prop;
            }
        };

        self.property = function(name, value, default_value) {
            if(value !== undefined) {
                return setProperty(name, value, normal);
            }
            else {
                var prop = getProperty(name, normal);
                if(prop == undefined && default_value != undefined)
                    return setProperty(name, default_value, normal);

                return prop;
            }
        };

        self.floatProperty = function(name, value, default_value) {
            return parseFloat(self.property(name, value, default_value));
        };

        self.intProperty = function(name, value, default_value) {
            return parseInt(self.property(name, value, default_value));
        };

        self.boolProperty = function(name, value, default_value) {
            var val = self.property(name, value, default_value);
            return val === "true" || val === true;
        };

        self.exists = function(name) {
            return getProperty(name, normal) !== undefined;
        };

        self.clear = function() {
            for(var key in data)
                if(key.indexOf(normal) == 0)
                    delete data[key];
        };
    }

    return new SaveData();
});
