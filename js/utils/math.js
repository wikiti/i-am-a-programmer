define(function() {
    var MathUtils = {
        clamp: function(val, min, max) {
            return val < min ? min : (val > max ? max: val)
        },
        randomInRange: function(min, max) {
            return min + (max - min) * Math.random();
        },
        randomIntInRange: function(min, max) {
            return Math.floor(this.randomInRange(min, max));
        },
        randomInList: function(list) {
            var index = this.randomIntInRange(0, list.length);
            return list[index];
        },
        randomInListWithWeights: function(list) {
            list = list.sort(function(a, b) { return a.weight - b.weight; });

            const total = list.reduce(function(acc, elem) { return acc + elem.weight; }, 0);
            var rand = this.randomInRange(0, total);
            var i = 0;

            while(rand > 0) {
                rand -= list[i].weight;
                if(rand <= 0) return list[i];
                i++;
            }

            return list[list.length - 1];
        },
        round: function(n) {
            return Math.round(n);
        }
    };

    return MathUtils;
});
