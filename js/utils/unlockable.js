define([
    "utils/save_data"
], function(SaveData) {
    function Unlockable(obj, callback) {
        // This method acts as an extender for a given object.

        // -- Private fields

        const self = obj;
        const enabled_property = `_${self.constructor.name.toLowerCase()}_enabled`;

        // -- Public fields

        self.onUnlocked = new Phaser.Signal();

        // -- Public functions

        self.unlock = function() {
            if(self.isLocked()) {
                SaveData.boolProperty(enabled_property, true);
                self.onUnlocked.dispatch(self);

                if(callback != undefined)
                    callback();
            }
        };

        self.isUnlocked = function() {
            return SaveData.boolProperty(enabled_property);
        };

        self.isLocked = function() {
            return !self.isUnlocked();
        };
    }

    return Unlockable;
});
