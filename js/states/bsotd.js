define([
    "utils/input",
    "utils/save_data"
], function(Input, SaveData) {
    function BSOTDState(game) {
        // -- Private fields

        const self = this;
        const clear_data_on_crash = false;
        const crash_sound_duration = 2500;
        const crash_image_duration = 2200;
        const bsotd_delay = 3000;

        var crash_sprite = null;
        var crash_sound = null;
        var bsotd_sprite = null;

        // -- Private functions

        var startCrashSound = function() {
            crash_sound = game.add.audio("broken1");
            crash_sound.loop = true;
            crash_sound.play();
        };

        var stopCrashSound = function() {
            crash_sound.destroy();
        };

        var runCrash = function() {
            crash_sprite = game.add.image(0, 0, "crash");
            startCrashSound();

            setTimeout(stopCrashSound, crash_sound_duration);
            setTimeout(stopCrash, crash_image_duration);
            setTimeout(runBSOTD, bsotd_delay);
        };

        var stopCrash = function() {
            crash_sprite.destroy();
        };

        var runBSOTD = function() {
            bsotd_sprite = game.add.sprite(0, 0, "bsotd");
            bsotd_sprite.animations.add("idle", [0, 1], 2, true);
            bsotd_sprite.animations.play("idle");

            addRestartEvents();
        };

        var addRestartEvents = function() {
            Input.onKeyUp.add(restartGame);
        };

        var restartGame = function() {
            if(clear_data_on_crash)
                SaveData.clear();

            // FIXME: Fix states loading to be able to reload from the beginning.
            // game.state.start("Boot");

            if(typeof location !== "undefined")
                location.reload();
        };

        // -- Public functions

        self.create = function() {
            runCrash();

            // Custom stuff
            SaveData.boolProperty("_dumb_hat_enabled", true);
        };

        self.shutdown = function() {
            Input.onKeyUp.remove(restartGame);
        };
    }

    BSOTDState.amIADummy = function() {
        return SaveData.boolProperty("_dumb_hat_enabled");
    };

    return BSOTDState;
});
