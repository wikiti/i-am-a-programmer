define([
    "graphics/gui/panel_factory",
    "graphics/gui/unlocked_endings",
    "utils/graphics",
    "utils/mechanics",
    "utils/save_data"
], function(PanelFactory, UnlockedEndings, Graphics, Mechanics, SaveData) {
    function MenuState(game) {
        // -- Private fields

        const self = this;
        const setup_done_property_name = "_setup_done";

        const title = {
            x: 0,
            y: -150,
            size: "80px",
            stroke: {
                color: Graphics.BLACK,
                size: 8
            },
            text: "I am a programmer".toUpperCase(),
            color: Graphics.WHITE
        };

        const subtitle = {
            x: -Graphics.SCREEN_WIDTH * 0.25 + 10,
            y: 70,
            size: '26px',
            stroke: {
                color: Graphics.BLACK,
                size: 0
            },
            text: "June 2017\nA minigame developed by Daniel \"Wikiti\" Herzog for a Molotov Studios' local contest.",
            color: Graphics.WHITE,
            lineSpacing: -10
        };

        const menu = {
            title: "Menu.exe",
            body: {
                new: "Welcome to your new life as a programmer.",
                continue: "Welcome back, programmer.",
                size: '23px'
            },
            button: {
                new: "Start",
                continue: "Continue"
            },
            x: 0,
            y: -60
        };

        var layer = null;

        var unlocked_endings = null;

        // -- Public fields

        self.guiLayer = null;

        // -- Private functions

        var isNewGame = function() {
            return !SaveData.boolProperty(setup_done_property_name);
        };

        var buttonText = function() {
            return isNewGame() ? menu.button.new : menu.button.continue;
        };

        var menuText = function() {
            return isNewGame() ? menu.body.new : menu.body.continue;
        };

        var setupLayers = function() {
            layer = game.add.group();
            Graphics.applyDefaultLayerOptions(layer);
            self.guiLayer = layer;
        };

        var setupBackgroundColor = function() {
            game.stage.backgroundColor = Graphics.CYAN;
        };

        var setupTitle = function() {
            var text = game.make.text(title.x, title.y, title.text, Graphics.textStyle);
            Graphics.applyDefaultGUIOptions(text);
            text.anchor.x = text.anchor.y = 0.5;
            text.fill = title.color;
            text.fontSize = title.size;
            text.strokeThickness = title.stroke.size;
            text.stroke = title.stroke.color;

            layer.addChild(text);
        };

        var setupSubtitle = function() {
            var text = game.make.text(subtitle.x, subtitle.y, subtitle.text, Graphics.textStyle);
            Graphics.applyDefaultGUIOptions(text);
            text.fill = subtitle.color;
            text.fontSize = subtitle.size;
            text.strokeThickness = subtitle.stroke.size;
            text.stroke = subtitle.stroke.color;
            text.lineSpacing = subtitle.lineSpacing;

            layer.addChild(text);
        };

        var setupMenu = function() {
            var panel = PanelFactory.generate(layer, PanelFactory.POPUP_SMALL, {
                title: menu.title,
                body: menuText(),
                body_size: menu.body.size,
                buttons: [
                    { text: buttonText(), callback: onFinish }
                ],
                mute: true,
                overlay_disabled: true
            });
            panel.show();
            panel.root.x = menu.x - panel.root.width * 0.5;
            panel.root.y = menu.y - panel.root.height * 0.5;
        };

        var onFinish = function() {
            game.state.start("Setup");
        };

        // -- Public functions

        self.create = function() {
            Mechanics.setState(self);

            setupLayers();
            setupBackgroundColor();
            setupTitle();
            setupSubtitle();
            setupMenu();

            unlocked_endings = new UnlockedEndings(self);
            unlocked_endings.show();
        };

        self.shutdown = function() {
            unlocked_endings.hide();
        };
    }

    return MenuState;
});
