define([
    "graphics/office_background",
    "graphics/office_desk",
    "graphics/programmer",
    "utils/input",
    "utils/graphics"
], function(OfficeBackground, OfficeDesk, Programmer, Input, Graphics) {
    function EndingState(game) {
        // -- Private fields

        const self = this;

        const animation = { time: 1200, easing: Phaser.Easing.Linear.None };

        const title_data = { x: 0, y: -170, text: "GAME OVER", size: '150px', thickness: 10 };
        const images_offset = { x: 20, y: -110 };
        const reason_text_data = { x: 0, y: -15, thickness: 0, size: '32px' };
        const reason_image_data = { x: 0, y: 44 };
        const reset_data = { x: 0, y: 70, size: '32px', thickness: 0,
                             text: "Press any key to reset the game." };

        const flash_delay = 400;
        const flash_duration = 200;
        const gameover_delay = 1800;
        const reason_delay = 4000;
        const reset_delay = 7000;
        const reset_event_delay = 7400;

        const reason_endings = {
            bugged: {
                text: "You got fired because you caused too many bugs."
            },
            bankrupt: {
                text: "You got fired because you ran out of budget."
            },
            lazy: {
                text: "You got fired because you did nothing on your first day."
            },
            true_programmer: {
                text: "You got fired because you are a TRUE programmer.\nWhat are you doing playing this game?"
            },
            grind: {
                text: "You got fired because you are no longer a useful slave."
            }
        };

        var final_type = null;

        var title = null;
        var programmer = null;
        var desk = null;
        var background = null;
        var music = null;


        // -- Public fields

        self.root = null;
        self.foregroundLayer = null;
        self.backgroundLayer = null;

        // -- Public functions

        var endingImageResource = function(name) {
            return "ending_" + name;
        };

        var setupOfficeBackground = function() {
            background = new OfficeBackground(self);
            background.show();
            background.defeat();

            background.root.x += images_offset.x;
            background.root.y += images_offset.y;
        };

        var setupProgrammerAndTable = function() {
            programmer = new Programmer(self);
            programmer.show(true);
            programmer.defeatAnimation();

            programmer.root.x += images_offset.x;
            programmer.root.y += images_offset.y;

            desk = new OfficeDesk(self);
            desk.show();

            desk.root.x += images_offset.x;
            desk.root.y += images_offset.y;
        };

        var setupLayers = function() {
            self.root = game.add.group();
            Graphics.applyDefaultLayerOptions(self.root);

            self.foregroundLayer = self.backgroundLayer = self.root;
        };

        var setupTitle = function() {
            title = game.make.text(title_data.x, title_data.y, title_data.text, Graphics.textStyle);
            Graphics.applyDefaultGUIOptions(title);
            title.anchor.x = title.anchor.y = 0.5;
            title.fontSize = title_data.size;
            title.strokeThickness = title_data.thickness;
            title.fill = Graphics.WHITE;

            self.foregroundLayer.addChild(title);
        };

        var setupAnimatedBlackScreen = function() {
            var black = game.add.graphics(0, 0);
            black.beginFill(Graphics.BLACK_HEX);
            black.drawRect(0, 0, Graphics.SCREEN_WIDTH, Graphics.SCREEN_HEIGHT);
            black.endFill();

            game.add.tween(black).to({ alpha: 0 }, animation.time, animation.easing, true);
        };

        var showGameOver = function() {
            setupOfficeBackground();
            setupTitle();
            setupProgrammerAndTable();

            setupAnimatedBlackScreen();

            music = game.sound.play("sadness");
        };

        var showPunchAnimation = function() {
            var white = game.add.graphics(0, 0);
            white.beginFill(Graphics.WHITE_HEX);
            white.drawRect(0, 0, Graphics.SCREEN_WIDTH, Graphics.SCREEN_HEIGHT);
            white.endFill();

            game.sound.play("hurt");

            game.time.events.add(flash_duration, function() { white.destroy(); });
        };

        var showReason = function() {
            // Show text reason
            var reason_text = game.make.text(reason_text_data.x, reason_text_data.y,
                                         reason_endings[type].text, Graphics.textStyle);
            Graphics.applyDefaultGUIOptions(reason_text);
            reason_text.anchor.x = title.anchor.y = 0.5;
            reason_text.fontSize = reason_text_data.size;
            reason_text.strokeThickness = reason_text_data.thickness;
            reason_text.fill = Graphics.WHITE;
            reason_text.align = "center";
            reason_text.lineSpacing = -8;

            self.foregroundLayer.addChild(reason_text);

            // Show image
            var reason_image = game.make.image(reason_image_data.x, reason_image_data.y,
                                               endingImageResource(type));
            Graphics.applyDefaultSpriteOptions(reason_image);
            self.foregroundLayer.addChild(reason_image);

            // Animate both
            reason_text.alpha = reason_image.alpha = 0;
            game.add.tween(reason_image).to({ alpha: 1.0 }, animation.time, animation.easing, true);
            game.add.tween(reason_text).to({ alpha: 1.0 }, animation.time, animation.easing, true);
        };

        var showReset = function() {
            var reset_text = game.make.text(reset_data.x, reset_data.y,
                                            reset_data.text, Graphics.textStyle);
            Graphics.applyDefaultGUIOptions(reset_text);
            reset_text.anchor.x = title.anchor.y = 0.5;
            reset_text.fontSize = reset_data.size;
            reset_text.strokeThickness = reset_data.thickness;
            reset_text.fill = Graphics.WHITE;

            self.foregroundLayer.addChild(reset_text);
            reset_text.alpha = 0;
            game.add.tween(reset_text)
                .to({ alpha: 1.0 }, animation.time, animation.easing, true, 0, 0, true)
                .loop(true);
        };

        var addResetHandler = function() {
            Input.onKeyUp.add(resetGame);
        };

        var resetGame = function() {
            // FIXME: Fix states loading to be able to reload from the beginning.
            // game.state.start("Boot");

            if(typeof location !== "undefined")
                location.reload();
        };

        // -- Public functions

        self.init = function(t) {
            type = t;
        };

        self.create = function() {
            setupLayers();

            game.time.events.add(flash_delay, showPunchAnimation);
            game.time.events.add(gameover_delay, showGameOver);
            game.time.events.add(reason_delay, showReason);
            game.time.events.add(reset_delay, showReset);
            game.time.events.add(reset_event_delay, addResetHandler);
        };

        self.shutdown = function() {
            music.destroy();
        };
    }

    return EndingState;
});
