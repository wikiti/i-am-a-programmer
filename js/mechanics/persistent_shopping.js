define([
   "mechanics/items_persistent/books",
   "mechanics/items_persistent/duck",
   "mechanics/items_persistent/lego",
   "mechanics/items_persistent/nes",
   "mechanics/items_persistent/poster",
   "mechanics/items_persistent/poster2",
   "mechanics/items_persistent/rubik",
   "mechanics/items_persistent/spinner",
   "mechanics/items_persistent/turret",

   "utils/save_data",
   "utils/unlockable"
], function(
    BooksItem,
    DuckItem,
    LegoItem,
    NESItem,
    PosterItem,
    Poster2Item,
    RubikItem,
    SpinnerItem,
    TurretItem,

    SaveData,
    Unlockable
) {
    function PersistentShopping(context) {
        // -- Private fields

        const self = this;
        const unlocked_property_name_prefix = "_persistent_item_unlocked";

        // Balance prices from this context
        const balance_multiplier = 10;
        const balance_extra = 0;

        // -- Public fields

        self.items = null;
        self.onItemUnlocked = new Phaser.Signal();
        self.onAllItemsUnlocked = new Phaser.Signal();

        // -- Private functions

        var unlockItem = function(item) {
            if(!self.isItemUnlocked(item)) {
                item.onUnlock(context.state);
                SaveData.property(unlockedPropertyName(item), true);
                self.onItemUnlocked.dispatch(item);

                var all_unlocked = true;
                self.items.forEach(function(item) {
                    if(!self.isItemUnlocked(item))
                        all_unlocked = false;
                });
                if(all_unlocked)
                    self.onAllItemsUnlocked.dispatch();
            }
        };

        var unlockedPropertyName = function(item) {
            return `${unlocked_property_name_prefix}_${item.name}`;
        };

        // -- Public functions

        self.purchase = function(item) {
            if(self.canBuy(item)) {
                unlockItem(item);
                context.creditsManaging.removeCredits(item.price, "purchase");
            }
        };

        self.create = function() {
            self.items = [
                new BooksItem(context),
                new DuckItem(context),
                new LegoItem(context),
                new NESItem(context),
                new PosterItem(context),
                new Poster2Item(context),
                new RubikItem(context),
                new SpinnerItem(context),
                new TurretItem(context)
            ];

            self.items.forEach(function(item) {
                item.price = Math.floor(item.price * balance_multiplier + balance_extra);
            });
        };

        self.isItemUnlocked = function(item) {
            return !!SaveData.property(unlockedPropertyName(item));
        };

        self.canBuy = function(item) {
            return context.creditsManaging.enoughCredits(item.price) &&
              !self.isItemUnlocked(item);
        };

        // -- Extend behaviour

        Unlockable(self);
    }

    return PersistentShopping;
});
