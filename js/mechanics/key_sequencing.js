define([
    "utils/graphics"
], function(Graphics) {
    function KeySequencing(context) {
        // -- Private fields

        const charCodes = {
            "a": Phaser.KeyCode.A,
            "b": Phaser.KeyCode.B,
            "c": Phaser.KeyCode.C,
            "d": Phaser.KeyCode.D,
            "e": Phaser.KeyCode.E,
            "f": Phaser.KeyCode.F,
            "g": Phaser.KeyCode.G,
            "h": Phaser.KeyCode.H,
            "i": Phaser.KeyCode.I,
            "j": Phaser.KeyCode.J,
            "k": Phaser.KeyCode.K,
            "l": Phaser.KeyCode.L,
            "m": Phaser.KeyCode.M,
            "n": Phaser.KeyCode.N,
            "o": Phaser.KeyCode.O,
            "p": Phaser.KeyCode.P,
            "q": Phaser.KeyCode.Q,
            "r": Phaser.KeyCode.R,
            "s": Phaser.KeyCode.S,
            "t": Phaser.KeyCode.T,
            "u": Phaser.KeyCode.U,
            "v": Phaser.KeyCode.V,
            "w": Phaser.KeyCode.W,
            "x": Phaser.KeyCode.X,
            "y": Phaser.KeyCode.Y,
            "z": Phaser.KeyCode.Z,
            " ": Phaser.KeyCode.SPACEBAR
        };

        const known_sequences = [
            {
                sequence: "give me money",
                callback: function() {
                    context.creditsManaging.addCredits(5000, "cheat");
                }
            },
            {
                sequence: "take my money",
                callback: function() {
                    context.creditsManaging.removeCredits(5000, "cheat");
                }
            },
            {
                sequence: "bug me a lot",
                callback: function() {
                    context.bugSpawning.spawn(15);
                }
            },
            {
                sequence: "i am a little penguin",
                callback: function() {
                    var penguin = context.game.make.sprite(-22, -26, "penguin_hat");
                    Graphics.applyDefaultSpriteOptions(penguin);
                    context.state.foregroundLayer.addChild(penguin);
                }
            },
            {
                sequence: "spam me plz",
                callback: function() {
                    context.randomEventCalling.findAndExecute("spam");
                }
            }
        ];

        const max_sequence_length = 40;
        const self = this;

        // -- Public fields

        self.history = [];

        // -- Private functions

        var stringToKeyCodes = function(string) {
            var codes = [];
            for(var i = 0; i < string.length; ++i)
              codes.push(charCodes[string.charAt(i)]);

            return codes;
        };

        var sequenceFound = function(string) {
            var codes = stringToKeyCodes(string);

            for(var j = 0; j < string.length; ++j) {
                // Do a reverse comparisson, from the last typed key, to the most left of
                // current sequence.
                if(self.history[self.history.length - 1 - j] != codes[codes.length - 1 - j])
                    return false;
            }

            return true;
        }

        var checkCurrentSequence = function() {
            known_sequences.forEach(function(obj) {
                if(sequenceFound(obj.sequence))
                    obj.callback();
            });
        };

        var onKeystroke = function(event) {
            self.history.push(event.keyCode);
            if(self.history.length > max_sequence_length) self.history.shift();

            checkCurrentSequence();
        };

        // -- Public functions

        self.register = function(sequence) {
            known_sequences.push(sequence);
        };

        self.create = function() {
            context.keyStroking.onKeystroke.add(onKeystroke);
        };
    }

    return KeySequencing;
});
