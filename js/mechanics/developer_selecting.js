define([
    "utils/input",
    "utils/save_data"
], function(Input, SaveData) {
    const developer_choice_key = "_choice_developer";

    function Developer(context, name, stats) {
        const self = this;

        self.name = name;
        self.stats = stats;

        self.apply = function() {
            for(var stat in self.stats)
                context.statsManaging[stat].extraValue(self.stats[stat]);

            SaveData.boolProperty(developer_choice_key, name);
        };

        self.filename = function() {
            return self.name.toLowerCase().replace(/[ \-\n]/g, '_');
        };
    }

    function DeveloperSelecting(context) {
        // -- Private fields

        const self = this;

        // -- Public fields

        self.options = null;

        // -- Private functions

        var initializeOptions = function() {
            self.options = [
                new Developer(context, "Full-stack\ndeveloper", {
                    productivity: 0.05,
                    happiness: 0.2,
                    concentration: 0.05
                }),
                new Developer(context, "Desktop\ndeveloper", {
                    productivity: 0.05,
                    happiness: 0.05,
                    concentration: 0.2
                }),
                new Developer(context, "Mobile\ndeveloper", {
                    productivity: 0.2,
                    happiness: 0.05,
                    concentration: 0.05
                }),
                new Developer(context, "Software\nengineer", {
                    productivity: 0.0,
                    happiness: 0.0,
                    concentration: 0.0
                })
            ];
        };

        // -- Public functions

        this.create = function() {
            initializeOptions();
        };
    }

    return DeveloperSelecting;
});
