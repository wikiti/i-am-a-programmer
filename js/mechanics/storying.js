define([
    "mechanics/story/bankrupt",
    "mechanics/story/bugged",
    "mechanics/story/lazy",
    "mechanics/story/normal",
    "mechanics/story/true_programmer",
    "utils/save_data"
], function(BankruptStory, BuggedStory, LazyStory, NormalStory, TrueProgrammerStory, SaveData) {
    function Storying(context) {
        // -- Private fields

        const self = this;
        const final_property_name = function(name) {
            return `_final_unlocked_${name}`;
        };

        const endings = [
            new NormalStory(context),
            new BuggedStory(context),
            new BankruptStory(context),
            new LazyStory(context),
            new TrueProgrammerStory(context)
        ];

        // -- Public fields

        self.endings = endings;

        // -- Private functions

        // ...

        // -- Public functions

        self.create = function() {
            // Do nothing
        };

        self.setup = function() {
            endings.forEach(function(ending) {
                ending.setup();
            });
        };

        self.triggerFinal = function(type) {
            SaveData.clear();
            SaveData.persistence(final_property_name(type), true);
            context.state.game.state.start("Ending", true, false, type);
        };

        self.isEndingUnlocked = function(type) {
            return !!SaveData.persistence(final_property_name(type));
        };
    }

    return Storying;
});
