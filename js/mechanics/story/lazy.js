define([
    "utils/save_data"
], function(SaveData) {
    function Lazy(context) {
        // -- Private fields

        const self = this;
        const final_name = "lazy";

        const initial_warning_delay = 30000;

        const warnings = [
            {
                time: 20000,
                text: "So... are you gonna do something? Write at least one line of code!"
            },
            {
                time: 20000,
                text: "I'm warning you! You better start coding right now!"
            }
        ]

        const property_name = "_lazy_did_something";

        var timer = null;
        var checks = 0;

        // -- Public fields

        self.name = final_name;

        // -- Private functions

        var setupTimer = function() {
            if(didSomething())
                return;

            context.game.time.events.add(initial_warning_delay, checkTimer);
        };

        var setupEvents = function() {
            context.coding.onLineCompleted.add(markAsDone);
        };

        var removeEvents = function() {
            context.coding.onLineCompleted.remove(markAsDone);
        };

        var markAsDone = function() {
            SaveData.boolProperty(property_name, true);
            removeEvents();
        };

        var didSomething = function() {
            return SaveData.boolProperty(property_name);
        };

        var checkTimer = function() {
            if(didSomething())
                return;

            var warning = warnings[checks];
            if(warning) {
                context.state.storyMessages.print(warning.text);
                context.game.time.events.add(warning.time, checkTimer);
            }
            else {
                execute();
            }

            checks += 1;
        };

        var execute = function() {
            context.storying.triggerFinal(final_name);
        };

        // -- Public functions

        self.setup = function() {
            setupEvents();
            setupTimer();
        };
    }

    return Lazy;
});
