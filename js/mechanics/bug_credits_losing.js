define(function() {
    function BugCreditsLosing(context) {
        // -- Private fields

        const self = this;
        const interval_time = 5000;
        const credits_per_bug = 20;

        var interval_timer = null;

        // -- Public fields

        // ...

        // -- Private functions

        var onInterval = function() {
            var count = context.bugSpawning.bugsPendingCount();

            if(count > 0 && context.bugSpawning.isUnlocked())
                context.creditsManaging.removeCredits(count * credits_per_bug, "bugs_loss");
        };

        var createTimer = function() {
            interval_timer = context.game.time.create(false);
            interval_timer.loop(interval_time, onInterval, self);
            interval_timer.start();
        };

        // -- Public functions

        self.pause = function() {
            interval_timer.pause();
        };

        self.resume = function() {
            if(interval_timer)
                interval_timer.resume();
            else
                createTimer();
        };

        self.create = function() {
            // Do nothing
        };
    }

    return BugCreditsLosing;
});
