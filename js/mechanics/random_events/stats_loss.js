define([
    "graphics/gui/panel_factory",
    "utils/math"
], function(PanelFactory, MathUtils) {
    function CreditsLoss(random_event_calling, context) {
        // -- Private fields

        const self = this;
        const messages = [
            {
                text: "There's a pig on the park, in front of the window.",
                stat: "concentration",
                value: 0.03
            },
            {
                text: "Your fish Fisher died. It was a really good friend.",
                stat: "happiness",
                value: 0.1
            },
            {
                text: "You need to stay 2 more hours at work to fix an annoying bug.",
                stat: "productivity",
                value: 0.04
            },
            {
                text: "Some popups appear in the browser with annoying music.",
                stat: "concentration",
                value: 0.1
            },
            {
                text: "Your latest CI build did not compile; you have to fix some typos.",
                stat: "happiness",
                value: 0.07
            },
            {
                text: "A client contacts you to fix some errors. Those 'errors' end up being " +
                      "some problems related to misuse of the system.",
                stat: "productivity",
                value: 0.08
            },
            {
                text: "Hey, look! A random popup event!",
                stat: "concentration",
                value: 0.09
            },
            {
                text: "LISP is now considered by GeekMagazine the best programming language ever made.",
                stat: "happiness",
                value: 0.08
            },
            {
                text: "A fly stood on your keyboard. You smash it. You broke your keyboard's shift key.",
                stat: "productivity",
                value: 0.07
            }
        ];

        var panel = null;

        // -- Public fields

        self.weight = 30;
        self.name = "stats_loss";

        // -- Private functions

        var loseMessage = function(data) {
            return `${data.text}\nYou lose ${Math.floor(data.value * 100)}% of ` +
                   `${context.statsManaging[data.stat].prettyName()}.`;
        };

        var calculateCredits = function(data) {
            var current = Math.floor(data.loss_ratio * context.creditsManaging.credits());
            return Math.max(min_credits_loss, current);
        };

        var onAccept = function() {
            random_event_calling.onEventSolved(true);
            panel.hide();
        };

        // -- Public functions

        this.execute = function() {
            var random = MathUtils.randomInList(messages);
            var stat = context.statsManaging[random.stat];

            panel = PanelFactory.generate(context.state.guiLayer, PanelFactory.POPUP_SHORT, {
                title: "Bloomer.exe",
                body: loseMessage(random),
                buttons: [{ text: "Accept", callback: onAccept }]
            });
            panel.show();
            panel.root.x = -panel.root.width * 0.5;
            panel.root.y += 40;

            stat.extraValue(stat.extraValue() - random.value);
        };
    }

    return CreditsLoss;
});
