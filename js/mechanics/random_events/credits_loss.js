define([
    "graphics/gui/panel_factory",
    "utils/math"
], function(PanelFactory, MathUtils) {
    function CreditsLoss(random_event_calling, context) {
        // -- Private fields

        const self = this;
        const percentage_ranges = { min: 0.1, max: 1.2 };
        const min_credits_loss = 100;
        const messages = [
            {
                text: "The production database of your application got wiped off.",
                loss_ratio: 0.4
            },
            {
                text: "When updating a vendor library, you application rolled back 100 purchases.",
                loss_ratio: 0.2
            },
            {
                text: "Pear company has sued you for using one of its patents.",
                loss_ratio: 1.1
            },
            {
                text: "Macrosoft company has sued you for using one of its patents.",
                loss_ratio: 0.6
            },
            {
                text: "Your financial manager has lost some of your money on a casino.",
                loss_ratio: 0.1
            },
            {
                text: "You're adopted.",
                loss_ratio: 0.1
            },
            {
                text: "You've been caught watching porn on job hours.",
                loss_ratio: 0.1
            },
            {
                text: "You've lost the credentials to access the production machine.",
                loss_ratio: 0.15
            },
            {
                text: "Your code is sexis because all objects has neutral/male names." +
                       "You've been sued by the LGTB community.",
                loss_ratio: 0.2
            }
        ];

        var panel = null;

        // -- Public fields

        self.weight = 30;
        self.name = "credits_loss";

        // -- Private functions

        var creditsMessage = function(message, credits) {
            return `${message}\nYou lose ${credits} $.`;
        };

        var calculateCredits = function(data) {
            var current = Math.floor(data.loss_ratio * context.creditsManaging.credits());
            return Math.max(min_credits_loss, current);
        };

        var onAccept = function() {
            random_event_calling.onEventSolved(true);
            panel.hide();
        };

        // -- Public functions

        this.execute = function() {
            var random = MathUtils.randomInList(messages);
            var credits = calculateCredits(random);

            panel = PanelFactory.generate(context.state.guiLayer, PanelFactory.POPUP_SHORT, {
                title: "Bloomer.exe",
                body: creditsMessage(random.text, credits),
                buttons: [{ text: "Accept", callback: onAccept }]
            });
            panel.show();
            panel.root.x = -panel.root.width * 0.5;

            context.creditsManaging.removeCredits(credits, "bloomer");
        };
    }

    return CreditsLoss;
});
