define([
    "graphics/gui/panel_factory",
    "utils/math"
], function(PanelFactory, MathUtils) {
    function Trade(random_event_calling, context) {
        // -- Private fields

        const self = this;
        const min_credits_cost = 150;

        const messages = [
            {
                text: function(credits) {
                    return `Do you want to purchase some bitcoins with ${credits}$? ` +
                        `They will make you happier!`;
                },
                credits_ratio: 0.1,
                stat: {
                    name: "happiness",
                    extra: 0.1
                }
            },
            {
                text: function(credits) {
                    return `If you buy this scheduler tool for only ${credits}$, ` +
                        `you'll be more productive!`;
                },
                credits_ratio: 0.15,
                stat: {
                    name: "productivity",
                    extra: 0.12
                }
            },
            {
                text: function(credits) {
                    return `Want to buy this $IMAGINATION_FOR_NAME_NOT_FOUND$ for only ` +
                        `${credits}$ to improve your concentration?`;
                },
                credits_ratio: 0.16,
                stat: {
                    name: "concentration",
                    extra: 0.14
                }
            }
        ];

        var panel = null;
        var last_selection;
        var last_credits;

        // -- Public fields

        self.name = "trade";
        self.weight = 30;

        // -- Private functions

        var calculateCredits = function(data) {
            var current = Math.floor(data.credits_ratio * context.creditsManaging.credits());
            return Math.max(min_credits_cost, current);
        };

        var enoughCredits = function(credits) {
            return context.creditsManaging.credits() >= credits;
        };

        var onAccept = function() {
            random_event_calling.onEventSolved(true);
            var extra_value = context.statsManaging[last_selection.stat.name].extraValue();
            context.statsManaging[last_selection.stat.name].extraValue(
                extra_value + last_selection.stat.extra
            );
            context.creditsManaging.removeCredits(last_credits, "event");
            panel.hide();
        };

        var onCancel = function() {
            random_event_calling.onEventSolved(false);
            panel.hide()
        };

        // -- Public functions

        this.execute = function() {
            last_selection = MathUtils.randomInList(messages);
            last_credits = calculateCredits(last_selection);

            panel = PanelFactory.generate(context.state.guiLayer, PanelFactory.POPUP_SHORT, {
                title: "Event.exe",
                body: last_selection.text(last_credits),
                buttons: [
                    { text: "Buy", callback: onAccept },
                    { text: "Cancel", callback: onCancel }
                ]
            });
            panel.show();
            panel.root.x = -panel.root.width * 0.5;

            if(!enoughCredits(last_credits))
                panel.buttons[0].disable();
        };
    }

    return Trade;
});
