define([
    "graphics/gui/panel_factory",
    "utils/math"
], function(PanelFactory, MathUtils) {
    function Tutorial(random_event_calling, context) {
        // -- Private fields

        const self = this;
        const credits = 200;
        const message = `This is a tutorial random event. By accepting it, you will earn ${credits}$`;

        var panel = null;

        // -- Public fields

        self.weight = 0;
        self.name = "tutorial";

        // -- Private functions

        var onAccept = function() {
            context.creditsManaging.addCredits(credits, "event");
            random_event_calling.onEventSolved(true);
            panel.hide();
        };

        // -- Public functions

        this.execute = function() {
            panel = PanelFactory.generate(context.state.guiLayer, PanelFactory.POPUP_SHORT, {
                title: "Event.exe",
                body: message,
                buttons: [{ text: "Accept", callback: onAccept }]
            });
            panel.show();
            panel.root.x = -panel.root.width * 0.5;
        };
    }

    return Tutorial;
});
