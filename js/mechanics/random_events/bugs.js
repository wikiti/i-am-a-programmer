define([
    "graphics/gui/panel_factory",
    "utils/math"
], function(PanelFactory, MathUtils) {
    function Bugs(random_event_calling, context) {
        // -- Private fields

        const self = this;
        const messages = [
            {
                text: "Shit! You've performer a division by zero!",
                count: 10
            },
            {
                text: "You've been asked to add a new technology related to Machine-Learning, " +
                       "which is not yet mature.",
                count: 15
            },
            {
                text: "A real bug has crept inside your computer.",
                count: 20
            },
            {
                text: "You poorly performed a git rebase a few moments ago, " +
                      "commiting stashed changes.",
                count: 25
            }
        ];

        var panel = null;

        // -- Public fields

        self.weight = 15;
        self.name = "bugs";

        // -- Private functions

        var message = function(data) {
            return `${data.text}\n${data.count} bugs have appeared.`;
        };

        var onAccept = function() {
            random_event_calling.onEventSolved(true);
            panel.hide();
        };

        // -- Public functions

        this.execute = function() {
            var random = MathUtils.randomInList(messages);

            panel = PanelFactory.generate(context.state.guiLayer, PanelFactory.POPUP_SHORT, {
                title: "Bugs.exe",
                body: message(random),
                buttons: [{ text: "Accept", callback: onAccept }]
            });
            panel.show();
            panel.root.x = -panel.root.width * 0.5;

            context.bugSpawning.spawn(random.count);
        };
    }

    return Bugs;
});
