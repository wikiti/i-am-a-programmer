define([
    "utils/math",
    "utils/save_data"
], function(MathUtils, SaveData) {
    // Stats have a name, a min (0.0) and a max (1.0)
    function Stat(manager, name) {
        // -- Private fields

        const self = this;
        const threshold_property_name = `_player_stat_${name}_threshold_value`;
        const extra_property_name = `_player_stat_${name}_extra_value`;

        const default_threshold_value = 0.1;
        const default_extra_value = 0.1;

        var threshold = 0; // Minimum value allowed
        var extra = 0; // Extra value over threshold. threshold + extra should be in range [0, 1]

        // -- Public fields

        self.name = name;
        self.onThresholdChange = new Phaser.Signal();
        self.onExtraChange = new Phaser.Signal();

        // -- Private functions

        var load = function() {
            extra = SaveData.floatProperty(extra_property_name, undefined, 0);
            threshold = SaveData.floatProperty(threshold_property_name, undefined, 0);
        };

        // -- Public functions

        self.prettyName = function() {
            return self.name.replace(/^./, function(m) { return m.toUpperCase(); });
        };

        self.totalValue = function() {
            return threshold + extra;
        };

        // Getter and setter
        self.extraValue = function(val) {
            if(val !== undefined) {
                extra = SaveData.floatProperty(
                    extra_property_name,
                    MathUtils.clamp(val, 0.0, 1.0 - threshold)
                );
                self.onExtraChange.dispatch(self, extra);
                manager.onExtraChange.dispatch(self, extra);
            }

            return extra;
        };

        // Getter and setter
        self.thresholdValue = function(val) {
            if(val !== undefined) {
                threshold = SaveData.floatProperty(
                    threshold_property_name,
                    MathUtils.clamp(val, 0, 1.0)
                );
                self.onThresholdChange.dispatch(self, threshold);
                manager.onThresholdChange.dispatch(self, threshold);

                if(threshold + extra > 1.0)
                    self.extraValue(1.0 - threshold);
            }

            return threshold;
        };

        self.reset = function() {
            self.thresholdValue(default_threshold_value);
            self.extraValue(default_extra_value);
        };

        self.create = function() {
            load();
        };
    }

    function StatsManaging(context) {
        // -- Private fields

        const self = this;

        // -- Public fields

        self.onThresholdChange = new Phaser.Signal();
        self.onExtraChange = new Phaser.Signal();

        self.productivity = new Stat(self, "productivity");
        self.happiness = new Stat(self, "happiness");
        self.concentration = new Stat(self, "concentration");

        self.stats = [
            self.productivity,
            self.happiness,
            self.concentration
        ];

        // -- Private functions

        // ...

        // -- Public functions

        self.reset = function() {
            self.stats.forEach(function(stat) {
                stat.reset();
            });
        };

        self.create = function() {
            self.stats.forEach(function(stat) { stat.create(); });
        };
    }

    return StatsManaging;
});
