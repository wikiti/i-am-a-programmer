define(function() {
    function Spinner(context) {
        // -- Private fields

        const self = this;
        var happiness_bonus = 0.15;

        // -- Public fields

        self.name = "spinner";
        self.price = 150;

        self.title = "Fidget Spinner";
        self.description = "One of those stupid things. This one is huge. Cannot be used. " +
                           "Well, it may be used as a ceiling fan. Improves happiness.";

        self.display = {
            x: -90,
            y: 44,
            layer: "backgroundLayer",
            image: "item_spinner"
        };

        // -- Public functions

        self.onUnlock = function() {
            var attr = context.statsManaging.happiness;
            attr.thresholdValue(attr.thresholdValue() + happiness_bonus);
        };
    }

    return Spinner;
});
