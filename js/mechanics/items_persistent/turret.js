define(function() {
    function Turret(context) {
        // -- Private fields

        const self = this;
        var concentration_bonus = 0.15;

        // -- Public fields

        self.name = "turret";
        self.price = 220;

        self.title = "Friendly turret";
        self.description = "A friendly turret which will make you company. " +
                           "With its new design, it shoot more bullets by shooting " +
                           "the whole bullet, including the case. " +
                           "Improves concentration.";

        self.display = {
            x: -54,
            y: -48,
            layer: "backgroundLayer",
            image: "item_turret"
        };

        // -- Public functions

        self.onUnlock = function() {
            var attr = context.statsManaging.concentration;
            attr.thresholdValue(attr.thresholdValue() + concentration_bonus);
        };
    }

    return Turret;
});
