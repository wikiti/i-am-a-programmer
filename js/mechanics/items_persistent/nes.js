define(function() {
    function NES(context) {
        // -- Private fields

        const self = this;
        var happiness_bonus = 0.2;

        // -- Public fields

        self.name = "nes";
        self.price = 500;

        self.title = "NES";
        self.description = "A fun and dangerous videoconsole to play with. " +
                           "Also called \"Nuclear Entertainment System\". Improves happiness.";

        self.display = {
            x: -108,
            y: -2,
            layer: "backgroundLayer",
            image: "item_nes"
        };

        // -- Public functions

        self.onUnlock = function() {
            var attr = context.statsManaging.happiness;
            attr.thresholdValue(attr.thresholdValue() + happiness_bonus);
        };
    }

    return NES;
});
