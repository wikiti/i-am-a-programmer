define([
    "utils/graphics"
], function(Graphics) {
    function Computer(state) {
        // -- Private fields

        const self = this;
        const position = { x: -52, y: -24 };

        var sprite;

        // -- Public fields

        // ...

        // -- Private functions

        var addSprite = function() {
            sprite = state.game.make.sprite(position.x, position.y, "computer");
            Graphics.applyDefaultSpriteOptions(sprite);
            sprite.anchor.x = sprite.anchor.y = 0;

            state.foregroundLayer.add(sprite);
        };

        // -- Public functions

        self.show = function() {
            addSprite();
        };
    }

    return Computer;
});
