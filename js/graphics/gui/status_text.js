define([
    "utils/graphics",
    "utils/math",
    "utils/mechanics"
], function(Graphics, MathUtils, Mechanics) {
    function StatusText(state) {
        // -- Private fields

        const self = this;
        const text_position_range = { x: { min: -100, max: 100 }, y: { min: 20, max: 220 } };
        const text_initial_position = { x: 0, y: 80 };
        const text_offset = { x: 0, y: -30 };
        const animation_time = 2500;
        const animation_easing = Phaser.Easing.Quadratic.In;

        const text_map = {
            bug: function(data) { return `${data} bug${data != 1 ? 's' : ''} remaining!`; },
            bugs_solved: function(data) { return "All bugs solved!"; },
            bugs_loss: function(data) { return `${data}$ - Pending bugs`; },
            loc: function(data) { return `${data}$ - Line of code`; },
            interval: function(data) { return `${data}$ - Idle time`; },
            purchase: function(data) { return `${data}$ - Purchase`; },
            bloomer: function(data) { return `${data}$ - Bloomer`; },
            stat: function(data) { return `${data} has changed`; },
            event: function(data) { return `${data}$ - Event` },
            cheat: function(data) { return `${data}$ - Cheater` }
        };

        // -- Public fields

        // ...

        // -- Private functions

        var addText = function(text, color) {
            var text_obj = state.game.make.text(0, 0, text, Graphics.textStyle);

            Graphics.applyDefaultGUIOptions(text_obj);
            state.guiLayer.addChild(text_obj);
            text_obj.anchor.x = text_obj.anchor.y = 0.5;
            text_obj.strokeThickness = 5;
            text_obj.lineSpacing = -15;
            text_obj.fontSize = '42px';

            text_obj.x = MathUtils.randomIntInRange(
                text_position_range.x.min,
                text_position_range.x.max
            );
            text_obj.y = MathUtils.randomIntInRange(
                text_position_range.y.min,
                text_position_range.y.max
            );
            text_obj.fill = color;
            text_obj.align = "center";

            return text_obj;
        };

        var colorByNumber = function(val) {
            return val > 0 ? Graphics.GREEN : Graphics.RED;
        };

        var pushText = function(data, template, color) {
            var text_val = template(data);
            var text = addText(text_val, color);
            var prop = {
                alpha: 0,
                x: text.x + text_offset.x,
                y: text.y + text_offset.y
            };

            state.game.add.tween(text).to(prop, animation_time, animation_easing, true)
              .onComplete.add(function() {
                 text.destroy();
              });
        };

        var pushCreditsChangeText = function(_total, val, reason) {
            pushText(val, text_map[reason], colorByNumber(val));
            state.game.sound.play("credits1");
        };

        var pushBlockedCoding = function(val, reason) {
            pushText(val, text_map[reason], Graphics.RED);
            state.game.sound.play("bug1");
        };

        var pushFixedBugs = function(_val, reason) {
            pushText(_val, text_map[reason], Graphics.GREEN);
            state.game.sound.play("credits1");
        };

        var pushStatChanged = function(stat) {
            pushText(stat.prettyName(), text_map.stat, Graphics.WHITE);
        };

        // -- Public functions

        self.show = function() {
            Mechanics.creditsManaging.onCreditsChange.add(pushCreditsChangeText);
            Mechanics.coding.onBlockedChange.add(pushBlockedCoding);
            Mechanics.bugSpawning.onAllBugsSolved.add(pushFixedBugs);
            Mechanics.statsManaging.onThresholdChange.add(pushStatChanged);
            Mechanics.statsManaging.onExtraChange.add(pushStatChanged);
        };
    }

    return StatusText;
});
