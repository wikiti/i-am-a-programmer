define([
    "utils/graphics",
    "utils/mechanics"
], function(Graphics, Mechanics) {
    function UnlockedEndings(state) {
        // -- Private fields

        const self = this;
        const position = { x: 0, y: 30 };

        const image_padding = 10;
        const image_offset = { x: 0, y: 0 };
        // const pending_text = "????";

        const text_data = { x: 0, y: 32, size: '26px', spacing: -15, stroke: 0, align: "center" };
        const title_data = { x: 0, y: -45, size: '48px', stroke: 0, text: "Unlocked Endings",
            alternative_text: "THANKS FOR PLAYING!"};

        const scale = 0.8;

        var group;
        var images_group;
        var game = state.game;

        // -- Public fields

        // ...

        // -- Private functions

        var setupTitle = function() {
            var text = game.make.text(title_data.x, title_data.y, title_data.text,
                Graphics.textStyle);
            Graphics.applyDefaultGUIOptions(text);
            text.anchor.x = text.anchor.y = 0.5;
            text.fontSize = title_data.size;
            text.strokeThickness = title_data.stroke;

            var all_unlocked = true;
            Mechanics.storying.endings.forEach(function(ending) {
                if(!Mechanics.storying.isEndingUnlocked(ending.name))
                    all_unlocked = false;
            });
            if(all_unlocked) {
                text.text = title_data.alternative_text;
                game.add.tween(text.scale).to({ x: text.scale.x * 1.5, y: text.scale.y * 1.5 }, 800,
                    Phaser.Easing.Sinusoidal.InOut, true).loop(true).yoyo(true);
            }

            group.addChild(text);
        };

        var setupGroup = function() {
            group = game.make.group();
            Graphics.applyDefaultLayerOptions(group);
            group.scale.x = group.scale.y = scale;
            group.x = position.x;
            group.y = position.y;
            state.guiLayer.addChild(group);
        };

        var setupEndings = function() {
            images_group = game.make.group();
            group.addChild(images_group);

            var acc = 0;
            Mechanics.storying.endings.forEach(function(ending) {
                var image = game.make.image(image_offset.x + acc, image_offset.y,
                    endingImageResource(ending));

                Graphics.applyDefaultSpriteOptions(image);
                images_group.addChild(image);

                var text = game.make.text(text_data.x, text_data.y, endingText(ending),
                    Graphics.textStyle);
                Graphics.applyDefaultGUIOptions(text);
                text.anchor.x = text.anchor.y = 0.5;
                image.addChild(text);
                text.fontSize = text_data.size;
                text.lineSpacing = text_data.spacing;
                text.strokeThickness = text_data.stroke;
                text.align = text_data.align;

                acc += image.width + image_padding;
            });

            // I'm fucking tired of positioning elements; let's just eyeball this shit.
            images_group.x = -images_group.width * 0.4;
        };

        var allLocked = function() {
            var all = true;
            Mechanics.storying.endings.forEach(function(ending) {
                all = all && !Mechanics.storying.isEndingUnlocked(ending.name);
            });
            return all;
        };

        var endingImageResource = function(ending) {
            if(Mechanics.storying.isEndingUnlocked(ending.name))
                return "ending_" + ending.name;
            else
                return "ending_pending";
        };

        var endingText = function(ending) {
            //if(!Mechanics.storying.isEndingUnlocked(ending.name))
            //    return pending_text;

            return ending.name.replace(/_([a-z])/g, function($0, $1) {
                return "\n" + $1.toUpperCase();
            }).replace(/^[a-z]/g, function($0) {
                return $0.toUpperCase();
            });
        };

        // -- Public functions

        self.show = function() {
            // Don't render anything if there are no endings.
            if(allLocked())
                return;

            setupGroup();
            setupTitle();
            setupEndings();
        };

        self.hide = function() {
            if(group != null)
                group.destroy();
        };
    }

    return UnlockedEndings;
});
