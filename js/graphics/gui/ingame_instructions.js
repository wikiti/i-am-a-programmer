define([
    "utils/graphics",
    "utils/mechanics"
], function(Graphics, Mechanics) {
    function IngameInstructions(state) {
        // -- Private fields

        const self = this;
        const help_position = { x: 120, y: 180 };

        var press_enter = {
            sprite: null,
            state: "press_enter",
            text: {
                body: "Next line!",
                offset: { x: 0, y: 35 }
            }
        };

        var press_keys = {
            sprite: null,
            state: "press_keys",
            text: {
                body: "Press random keys to code!",
                offset: { x: 0, y: 20 }
            }
        };

        var remove_bugs = {
            sprite: null,
            state: "remove_bugs",
            text: {
                body: "Crush those bugs!",
                offset: { x: 0, y: 20 }
            }
        };

        var current_mode = null;
        var layer = null;

        // -- Private functions

        var setupLayer = function() {
            layer = state.game.make.group();
            layer.x = help_position.x;
            layer.y = help_position.y;

            state.guiLayer.addChild(layer);
        };

        var setupPressEnter = function() {
            press_enter.sprite = state.game.make.sprite(0, 0, "gui_press_enter");
            Graphics.applyDefaultSpriteOptions(press_enter.sprite);
            press_enter.sprite.anchor.x = press_enter.sprite.anchor.y = 0.5;

            press_enter.sprite.animations.add("press", [0, 1], 3, true);
            press_enter.sprite.animations.play("press");

            var text = state.game.make.text(
                press_enter.text.offset.x, press_enter.text.offset.y,
                press_enter.text.body, Graphics.textStyle
            );
            Graphics.applyDefaultGUIOptions(text);
            text.fontSize = '28px';
            text.anchor.x = text.anchor.y = 0.5;
            text.fill = Graphics.WHITE;

            press_enter.sprite.addChild(text);
        };

        var setupPressKeys = function() {
            press_keys.sprite = state.game.make.sprite(0, 0, "gui_press_keys");
            Graphics.applyDefaultSpriteOptions(press_keys.sprite);
            press_keys.sprite.anchor.x = press_keys.sprite.anchor.y = 0.5;

            press_keys.sprite.animations.add("press", [0, 1, 2, 3, 4], 3, true);
            press_keys.sprite.animations.play("press");

            var text = state.game.make.text(
                press_keys.text.offset.x, press_keys.text.offset.y,
                press_keys.text.body, Graphics.textStyle
            );
            Graphics.applyDefaultGUIOptions(text);
            text.fontSize = '28px';
            text.anchor.x = text.anchor.y = 0.5;
            text.fill = Graphics.WHITE;

            press_keys.sprite.addChild(text);
        };

        var setupRemoveBugs = function() {
            remove_bugs.sprite = state.game.make.sprite(0, 0, "gui_bug");
            Graphics.applyDefaultSpriteOptions(remove_bugs.sprite);
            remove_bugs.sprite.anchor.x = remove_bugs.sprite.anchor.y = 0.5;

            remove_bugs.sprite.animations.add("remove", [8, 9], 2, true);
            remove_bugs.sprite.animations.play("remove");

            var text = state.game.make.text(
                remove_bugs.text.offset.x, remove_bugs.text.offset.y,
                remove_bugs.text.body, Graphics.textStyle
            );
            Graphics.applyDefaultGUIOptions(text);
            text.fontSize = '28px';
            text.anchor.x = text.anchor.y = 0.5;
            text.fill = Graphics.WHITE;

            remove_bugs.sprite.addChild(text);
        };

        var onUnlocked = function() {
            layer.visible = true;
        };

        var setupEvents = function() {
            Mechanics.coding.onLineChange.add(updateCurrentMode);
            Mechanics.coding.onLineCompleted.add(updateCurrentMode);
            Mechanics.bugSpawning.onNewBug.add(updateCurrentMode);
            Mechanics.bugSpawning.onBugSolved.add(updateCurrentMode);
            Mechanics.coding.onUnlocked.add(onUnlocked);
        };

        var updateCurrentMode = function() {
            // Check for remove bugs
            if(Mechanics.bugSpawning.bugsPending()) {
                if(current_mode == remove_bugs.state)
                    return;

                current_mode = remove_bugs.state;

                layer.removeChildren();
                layer.addChild(remove_bugs.sprite);
                remove_bugs.sprite.animations.stop();
                remove_bugs.sprite.animations.play("remove");
            }
            // Check for press enter
            else if(Mechanics.coding.currentLineFinished()) {
                if(current_mode == press_enter.state)
                    return;

                current_mode = press_enter.state;

                layer.removeChildren();
                layer.addChild(press_enter.sprite);
                press_enter.sprite.animations.stop();
                press_enter.sprite.animations.play("press");

            }
            // Check for press keys
            else if(!Mechanics.coding.currentLineFinished()) {
                if(current_mode == press_keys.state)
                    return;

                current_mode = press_keys.state;

                layer.removeChildren();
                layer.addChild(press_keys.sprite);
                press_keys.sprite.animations.stop();
                press_keys.sprite.animations.play("press");
            }
        };

        // -- Public functions

        self.show = function() {
            current_mode = null;

            setupLayer();
            setupPressEnter();
            setupPressKeys();
            setupRemoveBugs();

            setupEvents();

            updateCurrentMode();

            layer.visible = Mechanics.coding.isUnlocked();
        };
    }

    return IngameInstructions;
});
