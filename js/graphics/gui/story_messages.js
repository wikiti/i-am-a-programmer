define([
    "utils/graphics"
], function(Graphics) {
    function StoryMessages(state) {
        // -- Private fields

        const self = this;
        const default_message_duration_per_word = 400;
        const default_message_duration_base = 2800;

        const type_time_per_character = 40;
        const type_time_per_character_slow_factor = 5.0;
        const type_slow_characters = ",.!?;:";
        const type_sound = "type1";
        const type_sound_chars = /^[a-z0-9]$/i;

        const fade_in = { time: 400, easing: Phaser.Easing.Linear.None };
        const fade_out = { time: 500, easing: Phaser.Easing.Linear.None };

        const background_data = {
            alpha: 0.7,
            color: Graphics.BLACK_HEX,
            x: -Graphics.SCREEN_WIDTH*0.25,
            y: 185,
            width: Graphics.SCREEN_WIDTH * 0.5,
            height: Graphics.SCREEN_HEIGHT * 0.1
        };

        const text_data = {
            x: 55,
            y: 6,
            color: Graphics.WHITE,
            stroke: 0,
            font_size: '38px',
            max_width: Graphics.SCREEN_WIDTH * 0.8,
            line_spacing: -10
        };

        var messages_queue = null;
        var background = null;
        var text = null;
        var current_tween = null;

        // -- Public fields

        // ...

        // -- Private functions

        var setupBackground = function() {
            background = state.game.make.graphics(background_data.x, background_data.y);
            Graphics.applyDefaultSpriteOptions(background);
            background.beginFill(background_data.color, background_data.alpha);
            background.drawRect(0, 0, background_data.width, background_data.height);
            background.endFill();
            background.alpha = 0;

            state.guiLayer.addChild(background);
        };

        var setupText = function() {
            text = state.game.make.text(text_data.x, text_data.y, '',  Graphics.textStyle);
            Graphics.applyDefaultGUIOptions(text);
            text.addColor(text_data.color);
            text.strokeThickness = text_data.stroke;
            text.fontSize = text_data.font_size;
            text.wordWrap = true;
            text.wordWrapWidth = text_data.max_width;
            text.lineSpacing = text_data.line_spacing;

            background.addChild(text);
        };

        var defaultMessageDuration = function(message) {
            return default_message_duration_base + Math.floor(message.split(' ').length *
                default_message_duration_per_word);
        };

        var shiftNextMessage = function() {
            var message = messages_queue[0];

            state.game.tweens.removeFrom([text, background]);

            // We're done with the queue
            if(!message) {
                state.game.add.tween(background).to({ alpha: 0 }, fade_out.time, fade_out.easing, true);
                return;
            }

            text.alpha = 0;
            text.text = '';
            var tween = state.game.add.tween(text).to({ alpha: 1 }, fade_in.time, fade_in.easing, true);

            tween.onComplete.add(function() {
                setText(message.message);

                var tween_in = state.game.add.tween(text).to({ alpha: 0 }, fade_out.time, fade_out.easing);
                tween_in.delay(message.duration);
                tween_in.start();
                tween_in.onComplete.add(function() {
                    if(message.callback)
                        message.callback();

                    messages_queue.shift()
                    shiftNextMessage();
                });
            });
        };

        var setTextAnimated = function(string, i) {
            var char = string[i];
            if(!char)
                return;

            var time = type_time_per_character;
            if(type_slow_characters.indexOf(char) != -1)
                time *= type_time_per_character_slow_factor;

            if(char.match(type_sound_chars)) {
                state.game.sound.play(type_sound);
            }

            text.text += char;
            state.game.time.events.add(time, setTextAnimated, this, string, i + 1);
        };

        var setText = function(string) {
            var regex = /(?:\<((?:#[A-F0-9]+)|(?:CLEAR))\>)?([^<>]+)/g;
            var match;

            text.text = '';
            text.addColor(text_data.color, 0);

            final_text = '';

            while (match = regex.exec(string)) {
                if(match[1] == "CLEAR") {
                    text.addColor(text_data.color, text.text.length);
                }
                else if(match[1]) {
                    text.addColor(match[1], text.text.length);
                }

                final_text += match[2];
            }

            setTextAnimated(final_text, 0);
        };

        // -- Public functions

        self.show = function() {
            messages_queue = [];
            setupBackground();
            setupText();
        };

        self.print = function(message, duration, callback) {
            if(!duration)
                duration = defaultMessageDuration(message);

            var wasQueueEmpty = messages_queue.length == 0;

            messages_queue.push({
                message: message,
                duration: duration,
                callback: callback
            });

            if(wasQueueEmpty) {
                var tween = state.game.add.tween(background).to({ alpha: 1 }, fade_in.time, fade_in.easing, true);
                tween.onComplete.add(shiftNextMessage);
            }
        };
    }

    return StoryMessages;
});
