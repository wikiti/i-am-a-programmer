define([
    "graphics/gui/panel_factory",
    "utils/graphics",
    "utils/mechanics"
], function(PanelFactory, Graphics, Mechanics) {
    function DeveloperButton(game, parent, developer, callback) {
        // -- Private fields

        const self = this;
        const text_position = { x: -20, y: -2 };

        var text = null;

        // -- Public fields

        self.root = null;
        self.developer = developer;
        self.selected = false;

        // -- Private functions

        var setupBackground = function() {
            self.root = game.make.button(0, 0, `gui_developer_${developer.filename()}`);
            Graphics.applyDefaultSpriteOptions(self.root);

            self.root.animations.add("out", [0], Graphics.FPS, true);
            self.root.animations.add("over", [1], Graphics.FPS, true);
            self.root.animations.add("down", [2], Graphics.FPS, true);
            self.root.play("out");

            parent.addChildAt(self.root, 0);
        };

        var setupText = function() {
            text = game.make.text(0, 0, developer.name, Graphics.textStyle);
            Graphics.applyDefaultGUIOptions(text);
            self.root.addChild(text);

            text.anchor.x = text.anchor.y = 0.5;
            text.x = text_position.x;
            text.y = text_position.y;
            text.lineSpacing = -15;

            text.fontSize = '36px';
            text.fill = Graphics.BLACK;
            text.strokeThickness = 0;
        };

        var setupEvents = function() {
            self.root.onInputOver.add(onOver, this);
            self.root.onInputOut.add(onOut, this);
            self.root.onInputDown.add(onSelect, this);
        };

        var onSelect = function() {
            if(self.selected)
                return;

            self.setSelected(true);
        };

        var onOver = function() {
            if(self.selected)
                return;

            self.root.play("over");
        };

        var onOut = function() {
            if(self.selected)
                return;

            self.root.play("out");
        };

        // -- Public functions

        self.show = function() {
            setupBackground();
            setupText();
            setupEvents();
        };

        self.setSelected = function(value) {
            if(value) {
                callback(developer);
                self.root.play("down");
                text.fill = Graphics.WHITE;
            }
            else {
                self.root.play("out");
                text.fill = Graphics.BLACK;
            }

            self.selected = value;
        };
    }

    function DeveloperSelectorPanel(state) {
        // -- Private fields

        const self = this;
        const button_positions = [
            { x: 68, y: 70 },
            { x: 192, y: 70 },
            { x: 68, y: 124 },
            { x: 192, y: 124 }
        ];

        var onDoneCallback = null;
        var popup = null;
        var developers_buttons = [];

        // -- Public fields

        // ...

        // -- Private functions

        var onDone = function() {
            if(onDoneCallback)
                onDoneCallback();
        };

        var onPreSelect = function(developer) {
            developers_buttons.forEach(function(button) {
                button.setSelected(false);
            });
            developer.apply();
        };

        var setupPopup = function() {
            popup = PanelFactory.generate(state.guiLayer, PanelFactory.POPUP_LARGE, {
                title: "DeveloperSelection.exe",
                body: "Please, select a developer type:",
                overlay_disabled: true,
                buttons: [
                    { text: "Accept", callback: onDone, mute: true }
                ]
            });
            popup.show();
            popup.root.x = -popup.root.width * 0.5;
            popup.root.y = -popup.root.height * 0.92;
        }

        var initializeButtons = function() {
            Mechanics.developerSelecting.options.forEach(function(developer, i) {
                var button = new DeveloperButton(state.game, popup.root, developer, onPreSelect);
                button.show();
                button.root.x = button_positions[i].x;
                button.root.y = button_positions[i].y;

                developers_buttons.push(button);
            });

            // Set the first one as selected (default) since I don't want
            // to create a enabled/disabled button state.
            developers_buttons[0].setSelected(true);
        }

        // -- Public functions

        self.show = function(callback) {
            onDoneCallback = callback;
            setupPopup();
            initializeButtons();
        };

        self.hide = function(callback) {
            popup.root.destroy();

            if(callback)
                callback();
        };
    }

    return DeveloperSelectorPanel;
});
