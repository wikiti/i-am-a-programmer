define([
    "utils/graphics"
], function(Graphics) {
    function OfficeDesk(state) {
        // -- Private fields

        const self = this;
        const position = { x: -84, y: 17 };

        var sprite;

        // -- Public fields

        self.root = null;

        // -- Private functions

        var addSprite = function() {
            sprite = state.game.make.sprite(position.x, position.y, "office_desk");
            Graphics.applyDefaultSpriteOptions(sprite);
            sprite.anchor.x = sprite.anchor.y = 0;

            state.foregroundLayer.add(sprite);

            self.root = sprite;
        };

        // -- Public functions

        self.show = function() {
            addSprite();
        };
    }

    return OfficeDesk;
});
