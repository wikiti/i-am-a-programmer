define([
    "utils/graphics",
    "utils/mechanics"
], function(Graphics, Mechanics) {
    function ItemLoader(state) {
        // -- Private fields

        const self = this;

        // -- Private functions

        var showAllUnlockedItems = function() {
            Mechanics.persistentShopping.items.forEach(function(item, i) {
                if(Mechanics.persistentShopping.isItemUnlocked(item))
                    showItem(item);
            });
        };

        var showItem = function(item) {
            var image = state.game.make.image(item.display.x, item.display.y, item.display.image);
            Graphics.applyDefaultSpriteOptions(image);
            state[item.display.layer].addChild(image);
        };

        // -- Public functions

        self.show = function() {
            showAllUnlockedItems();
            Mechanics.persistentShopping.onItemUnlocked.add(showItem);
        };
    }

    return ItemLoader;
});
