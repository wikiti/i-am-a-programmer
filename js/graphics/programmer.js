define([
    "states/bsotd",
    "utils/graphics",
    "utils/math",
    "utils/mechanics"
], function(BSOTDState, Graphics, MathUtils, Mechanics) {
    function Programmer(state) {
        // -- Private fields

        const self = this;
        const position_base = { x: -52, y: -38 };
        const position_face = { x: -34, y: -39 };
        const tap_sound_delay = 100; // ms

        const blink_time_range = { min: 2000, max: 4000 }; // ms

        const dumb_hat_var = "_dumb_hat_enabled";

        var sprite_base;
        var sprite_face;

        var tap_sound_timer;
        var blink_timer;

        // -- Public fields

        self.root = null;

        // -- Private functions

        var blink = function() {
            if(blink_timer == null) {
                blink_timer = state.game.time.create(false);
                blink_timer.start();
            }
            else {
                blink_timer.stop();
                blink_timer.start();

                if(isFaceIdle())
                    sprite_face.animations.play("blink");
            }

            // Prepare next call
            blink_timer.add(
                MathUtils.randomIntInRange(blink_time_range.min, blink_time_range.max),
                blink
            );
        };

        var playTapSound = function() {
            if(tap_sound_timer == null) {
                tap_sound_timer = state.game.time.create(false);
                tap_sound_timer.start();
                state.game.sound.play("tap1");
            }
            else if(tap_sound_timer.ms > tap_sound_delay) {
                tap_sound_timer.stop();
                tap_sound_timer.start();
                state.game.sound.play("tap1");
            }
        };

        var addBaseSprite = function() {
            sprite_base = state.game.make.sprite(position_base.x, position_base.y, "programmer_base");
            Graphics.applyDefaultSpriteOptions(sprite_base);
            sprite_base.anchor.x = sprite_base.anchor.y = 0;
            state.foregroundLayer.add(sprite_base);

            // Setup animations
            sprite_base.animations.add("idle", [0], Graphics.FPS, true);
            sprite_base.animations.add("right_tap", [1, 2], Graphics.FPS, false)
                .onComplete.add(defaultBaseAnimation);
            sprite_base.animations.add("left_tap", [3, 4], Graphics.FPS, false)
                .onComplete.add(defaultBaseAnimation);
            sprite_base.animations.add("both_tap", [5, 6], Graphics.FPS, false)
                .onComplete.add(defaultBaseAnimation);

            sprite_base.animations.add("defeat", [7],  Graphics.FPS, true);

            defaultBaseAnimation();

            self.root = sprite_base;
        };

        var isBaseIdle = function() {
            return sprite_base.animations.currentAnim.name == "idle";
        };

        var isFaceIdle = function() {
            return sprite_face.animations.currentAnim.name == "idle";
        };

        var defaultBaseAnimation = function() {
            sprite_base.animations.play("idle");
        };

        var onCoding = function() {
            playTapSound();
            if(!isBaseIdle())
              return;

            var anims = [
                { value: "left_tap", weight: 3 },
                { value: "right_tap", weight: 3 },
                { value: "both_tap", weight: 1 }
            ];

            sprite_base.animations.play(
                MathUtils.randomInListWithWeights(anims).value
            );
        };

        var onCodingEnter = function() {
            playTapSound();
            sprite_base.animations.play("right_tap");
        };

        var addFaceSprite = function() {
            sprite_face = state.game.make.sprite(position_face.x, position_face.y, "programmer_face");
            Graphics.applyDefaultSpriteOptions(sprite_face);
            sprite_face.anchor.x = sprite_face.anchor.y = 0;
            state.foregroundLayer.add(sprite_face);

            sprite_face.animations.add("idle", [0], Graphics.FPS, true);
            var anim = sprite_face.animations.add("blink", [1], Graphics.FPS, false);
            anim.onComplete.add(defaultFaceAnimation);

            anim = sprite_face.animations.add("angry", [3], 1, false);
            anim.onComplete.add(defaultFaceAnimation);

            anim = sprite_face.animations.add("doubt", [2], 1, false);
            anim.onComplete.add(defaultFaceAnimation);

            anim = sprite_face.animations.add("happy", [4], 1, false);
            anim.onComplete.add(defaultFaceAnimation);

            anim = sprite_face.animations.add("proud", [5], 1, false);
            anim.onComplete.add(defaultFaceAnimation);

            sprite_face.animations.add("defeat", [6],  Graphics.FPS, true);

            defaultFaceAnimation();
        };

        var addDumbHat = function() {
            if(BSOTDState.amIADummy()) {
                var hat = state.game.make.sprite(-21, -46, "dumb_hat");
                Graphics.applyDefaultSpriteOptions(hat);
                state.foregroundLayer.addChild(hat);
            }
        };

        var defaultFaceAnimation = function() {
            sprite_face.animations.play("idle");
        };

        var angryFaceAnimation = function() {
            sprite_face.animations.play("angry");
        };

        var doubtFaceAnimation = function() {
            sprite_face.animations.play("doubt");
        };

        var happyFaceAnimation = function() {
            sprite_face.animations.play("happy");
        };

        var proudFaceAnimation = function() {
            sprite_face.animations.play("proud");
        };

        var addSprites = function() {
            addBaseSprite();
            addFaceSprite();
            addDumbHat();
        };

        var setupEvents = function() {
            Mechanics.coding.onLineChange.add(onCoding);
            Mechanics.coding.onLineCompleted.add(onCodingEnter);
            Mechanics.coding.onLineCompleted.add(doubtFaceAnimation);
            Mechanics.coding.onBlockedChange.add(angryFaceAnimation);

            Mechanics.bugSpawning.onNewBug.add(angryFaceAnimation);
            Mechanics.bugSpawning.onAllBugsSolved.add(happyFaceAnimation);

            Mechanics.persistentShopping.onItemUnlocked.add(proudFaceAnimation);

            blink();
        };

        // -- Public functions

        self.defeatAnimation = function() {
            sprite_face.animations.play("defeat");
            sprite_base.animations.play("defeat");
        };

        self.show = function(ignore_events) {
            addSprites();
            if(!ignore_events)
                setupEvents();
        };
    }

    return Programmer;
});
