# Notas

## Jugabilidad

- El juego trata sobre la vida de un programador.
- La acción de programar se llevará a cabo mediante la pulsación de teclas aleatorias del teclado.
  - Por cada línea de código, el jugador recibirá ingresos.
  - Cada línea de código cuenta con un número aleatorio (en un rango) caracteres pulsados.
  - Para evitar hacer trampas, no se permiten ciclos (ninguno de los últimos caracteres pulsados).
- Los "bugs" aparecen como bichos digitales en pantalla, que deberán ser aplastados (click) para resolverlos.
- Aparece texto de estado en pantalla de forma continua (-1 bug, +1 felicidad, etc).
- Para que sea más interesante, el jugador debería recibir ingresos cada cierto tiempo (iddle).
- El juego deberá tener **persistencia**; crear puntos de guardado entre sesiones.
- El programador hablará de vez en cuando, de forma aleatoria.
- ...

### Elecciones iniciales

- Se proponen una serie de elecciones iniciales al jugador:
  - Tipo de programador (web, escritorio, móvil). Afecta a estadísticas.
  - Lenguaje de programación (dar a escoger entre 3 o 4). Afecta a estadísticas.
  - Temática y nombre del proyecto.

### Eventos aleatorios

Eventos aleatorios:

- Popup o mensaje para introducir una nueva tecnologia prometiendo ganancias, pero creando perdidas y
  cientos de bugs en pantalla.
- Evento aleatorio para "Añadir extensión" para "cerrar esta página". Aparece una falsa pantalla azul y
  se reinicia el juego. Se añadirá audio personalizado para hacer más pesado el evento.
- Si el jugador se queda sin dinero, se muestra la oficina modo "pobre".

Los eventos aleatorios se generan cada cierto tiempo aleatorio, dentro de un intervalo predefinido, y
se seleccionará un evento de la lista completa según su peso.

Eventos predefinidos:

- Evento aleatorio que se ejecutará cuando el jugador escriba una secuencia determinada de caracteres.
  - Si el jugador escribe "i am a little and cute penguin", el jugador se transformará en un pingüino.
  - Si el jugador escribe "blue screen of death", aparecerá una pantalla azul de la muerte.
  - Si el jugador escribe "i am a programmer", aparecerá en pantalla un mensaje con texto "you are not".

### Estadísticas

- Las estadísticas varían de forma contínua, entre un mínimo (fijado por las elecciones iniciales) y
  un máximo (prestablecido).

Lista de estadísticas base:

- Productividad:
  - Dinero por línea de código.
- Felicidad:
  - Aumenta la probabilidad de que ocurran eventos aleatorios.
- Concentración:
  - Reduce la probabilidad de que ocurran bugs.

- El valor base (fijo) de la estadística puede aumentar, pero nunca disminuir. El valor
  superior (extra) puede aumentar y disminuir.
- El valor extra disminuye poco a poco a lo largo del tiempo.

### Objetos

- Se puede comprar objetos con mejoras. Los objetos comprados se muestran en la habitación del programador.

Lista de objetos temporales:

- Bebida energética BlueCow.
  - Aumenta ligeramente la productividad.
  - Disminuye ligeramente la concentración.
- Hamburguesa

Lista de objetos fijos (mejoras):

- Patito de goma.
  - Aumenta mucho la concentración base.
- Poster motivacional.
  - Aumenta la felicidad base.
- Consola recerativa (tipo NES).
  - Aumenta la felicidad base.
- Cubo de rubik.
  - Aumenta la productividad base.
- Torreta de Portal.
  - Aumenta la concentración base.
- Cabeza gigante de lego.
  - Aumenta la productivdad base.

### Ideas sueltas

- Dependiendo del lenguaje de programación que haya escogido, se mostrará a un personaje o a otro.
  - Esto implica usar sprites modulares.
- Crear un efecto de vibración o "Shake" cada vez que pulsamos una tecla.

## Assets

### Sprites

- Todo hecho a mano, con AseSprite.
- Fondo de juego estático, con ligeras animaciones.
- Pantalla de título simple y minimalista, con:
  - Título.
  - Subtítulo.
  - Desarrolladores.
  - Botón para empezar a jugar.

### Audio

- Hecho por Raquel, o extraído de algún host externo con licencia CC.

## Resources

- [Brainfuck](https://en.wikipedia.org/wiki/Brainfuck)
- [Pixel Artist Simulator](https://aamatniekss.itch.io/pixel-artist-simulator)
- [Soy programador](https://www.youtube.com/watch?v=OgIRAjnnJzI)
-
...
